var letter  = [ 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и',
                'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т',
                'у', 'ф', 'х', 'ц', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э',
                'ю', 'я'];
var sizeCell = 28;


function tableNM (n, m){
    var table = document.getElementById('table');
    var num = n * m;
    table.style.width = (n * sizeCell)+"px";
    table.style.height= (m * sizeCell)+"px";
    var bg = [],color = [];
    for (var i = 0; i < num; i++)
    {
        var div = document.createElement('div');
        bg = [Math.floor((Math.random() * 255) + 0), Math.floor((Math.random() * 255) + 0), Math.floor((Math.random() * 255) + 0)];
        color = [(255 -bg[0]), (255 -bg[1]), (255 -bg[1])];
        console.log(color);
        div.className = 'col';
        div.style.cssText = "width: "+(sizeCell - 1) +"px;" +
                            "height :" +(sizeCell - 1) +"px;"+
                            "background: rgb("+bg[0]+","+bg[1]+","+bg[2]+");"+
                            "color: rgb("+color[0]+","+color[1]+","+color[2]+");";
        div.innerHTML = letter[Math.floor((Math.random() * 32) + 0)];
        table.appendChild(div);
    }
}

function promptCheck (text){
    var num = prompt(text, String(Math.floor((Math.random() * 100) + 1)));
    if(num <1 || num > 100 ){
        return promptCheck(text);
    }
    return num;
}


window.onload = function(){
    var n, m;
    n = Math.floor((Math.random() * 63) + 1);
    m = Math.floor((Math.random() * 31) + 1);
    tableNM(n , m);

};