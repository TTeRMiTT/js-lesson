/**
 * Created by TTeRMiTT on 01.11.15.
 */

(function($){
	$(function(){
		//задание 1
		var i = 0;
		$("div>h1").each(function(index, h1){
			$(h1).text("Это новый заголовок "+(i+1));
			i++;
		});
		//задание 2
		$("form :text[name='fname']").val("Иван");
		$("form :text[name='lname']").val("Иванов");

		//Задание 3
		var select = $("#myselect option[value='2']");
		select.prop("selected", true);
		$("#myselect + div").text(select.text());

		//задание 4
		$("ul>li").eq(1).html("<b>" +$("ul>li").eq(1).text()+"</b>");

		//задание 5
		var liLast = $("ul>li:last");//взять последний li
		$("ul+div").text(liLast.text());
	});
})(jQuery);