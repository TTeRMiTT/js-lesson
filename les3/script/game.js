function Snake(){
	this.n = 30;
	this.size_cell = 10;
	this.snake_lenght =15;
	this.snake = [];
	this.fruit = {x:0, y:0};
	this.score = 0;
	this.move = "r";// возможные значения r - направо, l- налево, d - вниз, u- вверх
	this.delay_time = 200;
	this.g_loop = undefined;
	this.nwobj;

	this.createMatrix = function(){
		var size_cell = this.size_cell;
		var n = this.n;
		$("#matrix").each(function(index, matrix){
			$(matrix).css({
				width: (size_cell*n)+"px",
				height: (size_cell*n)+"px"
			});
			var y = 0;
			var x = 0;
			for (var i = 0; i < n*n; i++)
			{
				if(i%n == 0){
					y++;
				}
				x++;
				if (x > n){
					x = 1;
				}
				$(matrix).append("<div class='cell'>");
				$(".cell:last").addClass('cell row' + y+ ' col'+x).css({width: (size_cell)+"px", height: (size_cell)+"px"});
			}
		});
		$("#score").css({width: (size_cell*n) +"px", height: size_cell+"px"});
		this.viewScore();
	};

	this.viewScore = function(){
		var score_text = "Счет: "+this.score;
		$("#score").text(score_text);
		if (this.score % 5 == 0){
			if(this.delay_time > 50){
				this.delay_time = this.delay_time -10;
			}
			if(typeof this.g_loop != "undefined") clearInterval(this.g_loop);
			this.game_loop();
		}
	};

	this.game_loop = function(){
		var snake = this.nwobj;
		this.g_loop = setInterval(function(){
			if(typeof this.g_loop != "undefined") clearInterval(this.g_loop);
			$(document).keydown(function(event){
				switch (event.which){
					case 37:
						if (snake.move != "r"){
							snake.move = "l";
						}
						break;
					case 38:
						if (snake.move != "d"){
							snake.move = "u";
						}
						break;
					case 39:
						if (snake.move != "l"){
							snake.move = "r";
						}
						break;
					case 40:
						if (snake.move != "u"){
							snake.move = "d";
						}
						break;
				}
			});
			snake.moveSnake();
			snake.paintSnake();
		}, this.delay_time);
	};

	this.paintFruit = function(fruit){
		var light = $(".light");
		if(light.length !=0){
			light.removeClass("light");
		}
		this.fruit = fruit;
		$(".row"+fruit.y+".col"+fruit.x).addClass("light");
	};

	this.moveSnake = function(){
		var nx = this.snake[0].x;
		var ny = this.snake[0].y;
		switch (this.move){
			case "r":
				nx++;
				break;
			case "l":
				nx--;
				break;
			case "d":
				ny++;
				break;
			case "u":
				ny--;
				break;
		}

		if (this.fruit.x == nx && this.fruit.y == ny){
			this.score++;
			this.viewScore();
			this.createFruit();
		}
		this.snake.unshift({x:nx,y:ny});
		this.snake.pop();
	};

	this.paintSnake = function(){
		$(".head-snake").removeClass("head-snake");
		var sn = $(".snake");
		sn.removeClass("snake");
		var cl;
		for(var i= 0;i<this.snake_lenght;i++){
			if(i == 0){
				cl = "head-snake";
			}else{
				cl = "snake";
			}
			$(".row"+this.snake[i].y+".col"+this.snake[i].x).addClass(cl);
		}
		sn.css("width" , (this.size_cell)+"px");
	};

	this.init = function(){
		this.createSnake();
		this.createFruit();
		this.game_loop();
	};

	this.createSnake = function(){
		for(var i= this.snake_lenght;i >= 1;i--){
			this.snake.push({x: i,y:1})
		}
	};

	this.checkFruit = function(food){
		for(var i=0;i<this.snake_lenght;i++){
			if((food.x == this.snake[i].x) && (food.y == this.snake[i].y)){
				return false;
			}
		}
		return true;
	};

	this.createFruit = function(){
		var x = Math.floor((Math.random() * this.n) + 1);
		var y = Math.floor((Math.random() * this.n) + 1);
		if(this.checkFruit({x:x, y:y})){
			this.paintFruit({x:x, y:y});
		}else {
			this.createFruit();
		}
	};


}



(function($){
	$(function(){
		var snake = new Snake();
		snake.nwobj = snake;
		snake.createMatrix();
		snake.init();

	});
})(jQuery);
