/**
 * Created by Alexey_Muzalev on 30.10.2015.
 */
function erro(view, sr, val){
    if (val){
        view.innerText = "Вы не заполнили поле";
        sr.style.background = "red";

    }else{
        view.innerText = "";
        sr.style.background = "none";
    }

}


window.onload = function(){

    document.onkeydown = function(event){
        if(event.keyCode ==13){
            var view = document.getElementById("view");
            var str = document.getElementById("str");
            var clas = document.getElementById("class");
            var tag = document.getElementById("tag");
            if (str.value != ''){
                erro(view, str ,false);
                if(clas.value != ''){
                    erro(view, clas ,false);
                    if(tag.value != ''){
                        erro(view, tag ,false);
                        String.prototype.addToElement = function(tag, clas){
                            return "<" + tag + " class='" + clas + "'>" + this + "</" + tag + ">";
                        };
                        var str_new = String(str.value);
                        view.innerHTML = str_new.addToElement(tag.value, clas.value);
                    }else{
                        erro(view, tag ,true);
                    }
                }else{
                    erro(view, clas ,true);
                }
            }else {
                erro(view, str ,true);
            }
        }
    };
};