function MyHTML(){
    this.string = '';
    this.addText = function(string){
        this.string = this.string + string;
        return this.string;
    };

    this.addH = function(str, N){
       this.string = this.string + "<h"+N+">"+str+"</h"+N+">";
    };
    this.showHTML = function(){
        return this.string;
    }
}

window.onload = function(){
    var html  = new MyHTML();
    html.addText("<div>Это первая строка!</div>");
    html.addText("Это вторая строка!");
    html.addH("Это абзац!", 1);
    html.addText("Это последняя строка!");
    var str = html.showHTML();
    var body = document.body;
    body.innerHTML = str;
};

