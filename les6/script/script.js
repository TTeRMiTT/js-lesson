(function($){
	$(function(){
		var inputName = {   "Username" :"username", "Password" :"password",
							"Email" :"email", "Gender" :"gender",
							"Credit Card" :"credit_card", "Bio" :"bio"};
		$("#form").submit(function(event){
			$(".success").remove();
			$(".error-text").remove();
			$(".input").removeClass("error");
			$.post("validator", $(this).serializeArray() ,function(data){
				if (!data.result){
					for (var value in data.error){
						if (value == "Bio"){
							$("textarea[name="+inputName[value]+"]").parent().addClass("error").append("<span class='error-text'>"+data.error[value]+"</span>");
						}else{
							$("input[name="+inputName[value]+"]").parent().addClass("error").append("<span class='error-text'>"+data.error[value]+"</span>");
						}
					}
				}else{
					$("#form").append("<div class='success'>Все поля заполнены правельно</div>");
				}
			},"json");
			event.preventDefault();
		});
	});
})(jQuery);