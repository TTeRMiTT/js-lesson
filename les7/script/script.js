/**
 * Created by Alexey_Muzalev on 17.11.2015.
 */
(function($){
    $(function(){
        var inputName = {   "Username" :"username", "Password" :"password",
            "Email" :"email", "Gender" :"gender",
            "Credit Card" :"credit_card", "Bio" :"bio", "Birth": "birth"};
        $("#date").datepicker({
            dateFormat: 'yy-mm-dd',
            yearRange: "1950:2015",
            changeYear: true,
            changeMonth: true
        });
        $("#form").submit(function(event){
            $(".ui-dialog").remove();
            var cont = 0;
            $(".success").remove();
            $(".error-text").remove();
            $(".input").removeClass("error");
            $.post("validator.php", $(this).serializeArray() ,function(data){
                if (!data.result){
                    for (var value in data.error){
                        if (value == "Bio"){
                            $("textarea[name="+inputName[value]+"]").effect("bounce",7000).parent().append("<div id='"+inputName[value]+"'></div>");
                                $("#"+inputName[value]).dialog({
                                    title: data.error[value]
                                });
                        }else{
                            $("input[name="+inputName[value]+"]").effect("bounce",7000).parent().append("<div id='"+inputName[value]+"'></div>");
                            $("#"+inputName[value]).dialog({
                                title: data.error[value]
                            });
                        }
                        $("div[aria-describedby='"+inputName[value]+"']").appendTo("#"+inputName[value]+"-rt");
                    }
                    cont = (7 - Object.keys(data.error).length)*15;
                }else{
                    $("#form").append("<div class='success'>Все поля заполнены правельно</div>");
                    cont = 100;
                }
                $("#progressbar").progressbar({
                    value: cont
                });
            },"json");
            event.preventDefault();
        });
    });
})(jQuery);
